﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurGames.QuestionTable
{
    public interface IQuestionTableCell
    {
        /// <summary>
        /// Ранг вопроса, если он равен 0 - то это тема группы, иначе - сложность вопроса. Изменяется от 1 до 5
        /// </summary>
        byte QuestionRank { get; set; }
        /// <summary>
        /// Этап игры, в зависисмости от которого число очков другое
        /// </summary>
        byte GameStage { get; set; }

        /// <summary>
        /// Надпись в ячейке: очки для вопроса, и формулировка темы для группы вопросов
        /// </summary>
        string CellTitle { get; set; }



    }
}
