﻿using OurGames.QuestionController;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OurGames.QuestionTable
{
    public class QuestionTable : IDisposable
    {
        private readonly ushort LimitWidth = 1024;
        private readonly ushort LimitHeight = 768;

        /// <summary>
        /// Двумерный массив вопросов.
        /// </summary>
        IQuestionTableCell[,] QuestionTableCells { get; set; } = null;
        /// <summary>
        /// Ссылка на форму=)
        /// </summary>
        Form HeadForm { get; set; } = null;
        QuestionTableDrawer TableDrawer { get; set; } = null;
        public QuestionTable(Form headForm, QuestionList questionList)
        {
            this.TableDrawer = new QuestionTableDrawer();
            this.HeadForm = headForm;
            this.HeadForm.Resize += DrawTableQuestion;
            this.HeadForm.MouseClick += TableClickListener;
            this.HeadForm.KeyUp += EscKeyWait;


        }

        private void EscKeyWait(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DrawTableQuestion(this.HeadForm, EventArgs.Empty);
            }
        }
        /// <summary>
        /// Перерисовка таблицы вопросов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DrawTableQuestion(object sender, EventArgs e)
        {
            if (this.HeadForm.Width < LimitWidth) this.HeadForm.Width = LimitWidth;
            if (this.HeadForm.Height < LimitHeight) this.HeadForm.Height = LimitHeight;
            this.TableDrawer.DrawTableQuestion(this.HeadForm);
        }

        private void TableClickListener(object sender, MouseEventArgs e)
        {
            float localX = e.X - this.TableDrawer.WidthQuestionPart;
            if (localX > 0) //Если нажали на вопросы, то считаем Y
            {
                float localY = e.Y;
                int concretRow = (int)(e.Y / this.TableDrawer.TableRowSize);
                int concretCol = (int)(localX / this.TableDrawer.WidthQuestionCell);

                /*
                MessageBox.Show("Координаты выбранной ячейки. X: " + concretCol.ToString() + "; Y: " + concretRow.ToString(),
                    "Внимание",  MessageBoxButtons.OK, MessageBoxIcon.Information);
                */

                this.TableDrawer.DrawQuestion(this.HeadForm, new Question("Если ты хочешь вообще на любом рисунке найти надпись и узнать ее размеры, " +
                    "то тут намного сложнее. Тебе нужно копать в сторону OCR. " +
                    "А именно брать различные куски рисунка и преобразовывать их в текст, если получил текст, то вычисляй размер"));
            }
        }

        public void Dispose()
        {
            this.HeadForm.Resize -= DrawTableQuestion;
            this.HeadForm.MouseClick -= TableClickListener;
        }
    }
}
