﻿using OurGames.QuestionController;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OurGames.QuestionTable
{
    public class QuestionTableDrawer
    {
        private Color BackgroundColor = Color.DarkBlue;
        private Color BorderColor = Color.Yellow;
        private byte WidthBorder = 5;
        /// <summary>
        /// Соотношение частей 1:2
        /// </summary>
        private readonly byte QuestionPart = 1;
        private readonly byte CellPart = 2;


        private readonly byte CategoryNum = 5;
        private readonly byte NumQuestionCell = 1;
        private readonly byte QuestionGroupNum = 5;

        public QuestionTableDrawer()
        {

        }

        public float TableRowSize { get; private set; }
        /// <summary>
        /// Поле для контроля ширины части, содержащей вопрос
        /// </summary>
        public float WidthQuestionPart { get; private set; }

        public float WidthCellPart { get; private set; }

        public float WidthQuestionCell { get; private set; }

        public int FormHeight { get; set; }

        public int FormWidth { get; set; }


        public void DrawTableQuestion(Form headForm)
        {
            this.FormWidth = headForm.Width;
            this.FormHeight = headForm.Height;
            this.TableRowSize = (float)(this.FormHeight - (this.CategoryNum - 1) * this.WidthBorder) / this.CategoryNum;
            this.WidthQuestionPart = (this.FormWidth / (this.CellPart + this.QuestionPart)) * this.QuestionPart;
            this.WidthCellPart = (this.FormWidth / (this.CellPart + this.QuestionPart)) * this.CellPart;
            this.WidthQuestionCell = (float)(this.WidthCellPart) / this.QuestionGroupNum;

            Graphics g = headForm.CreateGraphics();
            g.Clear(BackgroundColor);
            for (int i = 1; i < this.CategoryNum; i++)
                g.DrawLine(new Pen(BorderColor, WidthBorder), 0, i * this.TableRowSize, this.FormWidth, i * this.TableRowSize);

            for (int i = 0; i < this.QuestionGroupNum; i++)
                g.DrawLine(new Pen(BorderColor, WidthBorder), this.WidthQuestionPart + i * this.WidthQuestionCell, 0,
                    this.WidthQuestionPart + i * this.WidthQuestionCell, this.FormHeight);
        }

        public void DrawQuestion(Form headForm, Question question)
        {
            SolidBrush drawBrush = new SolidBrush(Color.Yellow);
            Font f = new Font(headForm.Font, FontStyle.Bold);
            Font drawFont = new Font("Arial", 26, FontStyle.Bold);
            Graphics g = headForm.CreateGraphics();
            g.Clear(BackgroundColor);
            var questionText = QuestionFormatter(question.QuestionText, ref drawFont);
            if (questionText == null)
            {
                g.DrawString("Error", drawFont, drawBrush, GetCentrStringCoord("Error", drawFont, 0, 1));
                //this.DrawTableQuestion(headForm);
                return;
            }
            int startTextCoord = CalcStartTextCoord(drawFont, questionText.Count);
            for (int i=0; i < questionText.Count; i++)
            {
                Point location = GetCentrStringCoord(questionText[i], drawFont, i, questionText.Count);
                g.DrawString(questionText[i], drawFont, drawBrush, location.X, startTextCoord + location.Y);
            }
        }

        private Point GetCentrStringCoord(string text, Font font, int strokeNum, int strokeAll)
        {
            Size textSize = TextRenderer.MeasureText(text, font);
            Point centrForm = new Point(this.FormWidth / 2, (int)(strokeNum * (textSize.Height)));
            return new Point((int)(centrForm.X - textSize.Width / 2), centrForm.Y);
        }

        private int CalcStartTextCoord(Font font, int strokeNum)
        {
            Size textSize = TextRenderer.MeasureText("Template", font);
            int textHeight = textSize.Height * strokeNum;
            return this.FormHeight / 2 - textHeight / 2;
        }
        private List<string> QuestionFormatter(string question, ref Font curFont)
        {
            List<string> questionRow = new List<string>();
            var words = this.GetWords(question);
            if (words.Count == 0) return null;

            string curRow = "";
            for (int i=0; i < words.Count; i++)
            {
                curRow += words[i] + " ";
                if (TextRenderer.MeasureText(curRow, curFont).Width >= (this.FormWidth * 2 / 3))
                {
                    questionRow.Add(curRow);
                    curRow = "";
                }
            }
            questionRow.Add(curRow);
            curRow = "";
            return questionRow;
        }

        private List<string> GetWords(string question)
        {
            List<string> words = new List<string>();
            string curWord = "";
            for (int i=0; i < question.Length; i++)
            {
                if (question[i].Equals(' '))
                {
                    words.Add(curWord);
                    curWord = "";
                }
                else
                    curWord += question[i];
            }
            words.Add(curWord);
            //Проверка слов
            for (int i=0; i < words.Count; i++)
            {
                if (words[i].Length < 2)
                {
                    if (words[i].Length == 0 || words[i].Equals(" "))
                    {
                        words.RemoveAt(i);
                        i--;
                    }
                }
            }
            return words;
        }

    }
}
