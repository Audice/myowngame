﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OurGames.QuestionController
{
    public class Question
    {
        //Текст вопроса
        public string QuestionText { get; private set; }

        public Question(string questionText)
        {
            this.QuestionText = questionText;
        }

    }
}
