﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OurGames
{
    public partial class Form1 : Form
    {
        QuestionTable.QuestionTable QuestionTable { get; set; } = null;
        public Form1()
        {
            InitializeComponent();
            this.QuestionTable = new QuestionTable.QuestionTable(this, null);           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
